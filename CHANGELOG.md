## [5.4.8](https://gitlab.com/to-be-continuous/terraform/compare/5.4.7...5.4.8) (2024-04-06)


### Bug Fixes

* **gcp:** setup GCP credentials through ADC; get rid of gcp-auth-provider ([c7b6d21](https://gitlab.com/to-be-continuous/terraform/commit/c7b6d21190b2b0f8f2292fbdeabcd0982b586cae))

## [5.4.7](https://gitlab.com/to-be-continuous/terraform/compare/5.4.6...5.4.7) (2024-04-06)


### Bug Fixes

* **plan:** don't lock state when generating tf plan from MR ([b61219f](https://gitlab.com/to-be-continuous/terraform/commit/b61219f59b5a826b7d6a9f1ecb884b978bcbec20))

## [5.4.6](https://gitlab.com/to-be-continuous/terraform/compare/5.4.5...5.4.6) (2024-04-06)


### Bug Fixes

* **plan:** expire tf plan artifact after one day ([b8fb76d](https://gitlab.com/to-be-continuous/terraform/commit/b8fb76dc1699db0810934d4037903d93a8cc9920)), closes [#67](https://gitlab.com/to-be-continuous/terraform/issues/67)

## [5.4.5](https://gitlab.com/to-be-continuous/terraform/compare/5.4.4...5.4.5) (2024-04-03)


### Bug Fixes

* **variants:** use service containers "latest" image tag (Vault, GCP) ([ae811fb](https://gitlab.com/to-be-continuous/terraform/commit/ae811fbed74187a2c1cae6d7e19353df0c1bfd1d))

## [5.4.4](https://gitlab.com/to-be-continuous/terraform/compare/5.4.3...5.4.4) (2024-04-02)


### Bug Fixes

* remove extra '\n' in netcated HTTP query ([618b6b9](https://gitlab.com/to-be-continuous/terraform/commit/618b6b90e0f0ca8c3fa0530e7621d25442106923)), closes [#73](https://gitlab.com/to-be-continuous/terraform/issues/73)

## [5.4.3](https://gitlab.com/to-be-continuous/terraform/compare/5.4.2...5.4.3) (2024-04-02)


### Bug Fixes

* all jobs should extend .tf-base ([c21e399](https://gitlab.com/to-be-continuous/terraform/commit/c21e399c1d49f10654a113a5d586bbbf2d2621dd))

## [5.4.2](https://gitlab.com/to-be-continuous/terraform/compare/5.4.1...5.4.2) (2024-03-24)


### Bug Fixes

* add write check permission before updating ca certs ([98a0318](https://gitlab.com/to-be-continuous/terraform/commit/98a031860c57e58728061779dc9586fd59494fd2))

## [5.4.1](https://gitlab.com/to-be-continuous/terraform/compare/5.4.0...5.4.1) (2024-03-20)


### Bug Fixes

* **tf-publish-module:** add an explicit message on curl error ([5cf5b08](https://gitlab.com/to-be-continuous/terraform/commit/5cf5b0861d3662a65acdcf22a74eb176dd120fc6))

# [5.4.0](https://gitlab.com/to-be-continuous/terraform/compare/5.3.0...5.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([93c29a2](https://gitlab.com/to-be-continuous/terraform/commit/93c29a2749c2d115efbe2c4c5de10aa52f9f01b4))

# [5.3.0](https://gitlab.com/to-be-continuous/terraform/compare/5.2.0...5.3.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([8fabac4](https://gitlab.com/to-be-continuous/terraform/commit/8fabac4df794503bee3462a36db5df5abae520ba))

# [5.2.0](https://gitlab.com/to-be-continuous/terraform/compare/5.1.0...5.2.0) (2023-12-3)


### Features

* support private registries authentication ([0e374c0](https://gitlab.com/to-be-continuous/terraform/commit/0e374c07cc9d0757079f7613fdd67b86858c1e19))

# [5.1.0](https://gitlab.com/to-be-continuous/terraform/compare/5.0.2...5.1.0) (2023-11-21)


### Features

* add AWS OIDC variant ([5375866](https://gitlab.com/to-be-continuous/terraform/commit/53758666b15e9f152abee4a9dbd1365fd2dfae4f))

## [5.0.2](https://gitlab.com/to-be-continuous/terraform/compare/5.0.1...5.0.2) (2023-11-18)


### Bug Fixes

* applying the plan from the plan job ([07c42bc](https://gitlab.com/to-be-continuous/terraform/commit/07c42bc1bfd9aefae562c9b78e5f861eb102ad14))

## [5.0.1](https://gitlab.com/to-be-continuous/terraform/compare/5.0.0...5.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([38d4391](https://gitlab.com/to-be-continuous/terraform/commit/38d4391703a656bfa0336bd532566e99a32db780))

# [5.0.0](https://gitlab.com/to-be-continuous/terraform/compare/4.0.3...5.0.0) (2023-09-26)


* feat!: support environment auto-stop ([244cc49](https://gitlab.com/to-be-continuous/terraform/commit/244cc49143abafa56f91dde2d520db3cbfbfbb5e))


### BREAKING CHANGES

* now review environments will auto stop after 4 hours
by default. Configurable (see doc).

## [4.0.3](https://gitlab.com/to-be-continuous/terraform/compare/4.0.2...4.0.3) (2023-09-19)


### Bug Fixes

* **checkov:** remove directory from configurable checkov args ([371b583](https://gitlab.com/to-be-continuous/terraform/commit/371b5838df75846fe730ec29dc0fe6fd635ede5e))

## [4.0.2](https://gitlab.com/to-be-continuous/terraform/compare/4.0.1...4.0.2) (2023-09-19)


### Bug Fixes

* **tflint:** run tflint init to install modules ([1fd9090](https://gitlab.com/to-be-continuous/terraform/commit/1fd909011c4dad708b5872875f8bb48a7fdb1bc8))

## [4.0.1](https://gitlab.com/to-be-continuous/terraform/compare/4.0.0...4.0.1) (2023-09-06)


### Bug Fixes

* remove .terraform.lock.hcl from cache ([0c13bff](https://gitlab.com/to-be-continuous/terraform/commit/0c13bfff6a7c46ac20f5ea6b41a97bcd009e4521))

# [4.0.0](https://gitlab.com/to-be-continuous/terraform/compare/3.12.3...4.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([89dfcb5](https://gitlab.com/to-be-continuous/terraform/commit/89dfcb5002be84b4b116522ef05e0e4808c35004))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

## [3.12.3](https://gitlab.com/to-be-continuous/terraform/compare/3.12.2...3.12.3) (2023-08-18)


### Bug Fixes

* configure http backend via exported variables only ([0513532](https://gitlab.com/to-be-continuous/terraform/commit/0513532f72f8f6bb9a9700cd42dc645ab8c15499))

## [3.12.2](https://gitlab.com/to-be-continuous/terraform/compare/3.12.1...3.12.2) (2023-06-15)


### Bug Fixes

* **url:** revert to wget method and fallback to netcat on error for busybox ([02a7096](https://gitlab.com/to-be-continuous/terraform/commit/02a7096033f1bfa2ea728e26df0a7ed731eb3509))

## [3.12.1](https://gitlab.com/to-be-continuous/terraform/compare/3.12.0...3.12.1) (2023-06-15)


### Bug Fixes

* **image:** update TF_IMAGE default value from light to latest ([0b9437f](https://gitlab.com/to-be-continuous/terraform/commit/0b9437f2e5352e17c5633f881166f2b41ae0c3a9))

# [3.12.0](https://gitlab.com/to-be-continuous/terraform/compare/3.11.0...3.12.0) (2023-06-12)


### Bug Fixes

* **hooks:** chmod only when required ([0dd1c70](https://gitlab.com/to-be-continuous/terraform/commit/0dd1c704dcd571fd0721efbae5bff65ee255aa86))


### Features

* **tf-publish:** add terraform module publish support ([e7c245d](https://gitlab.com/to-be-continuous/terraform/commit/e7c245d09ada5e968aafc741912595ef7dd60f7e))

# [3.11.0](https://gitlab.com/to-be-continuous/terraform/compare/3.10.0...3.11.0) (2023-05-30)


### Features

* **tf-docs:** add terraform docs build support ([bc9aaba](https://gitlab.com/to-be-continuous/terraform/commit/bc9aaba0af90f61b783f83c35504514fd9d17e9a))

# [3.10.0](https://gitlab.com/to-be-continuous/terraform/compare/3.9.1...3.10.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([1ec2316](https://gitlab.com/to-be-continuous/terraform/commit/1ec2316dd247e8a95d3def7dd96640f66a67289b))

## [3.9.1](https://gitlab.com/to-be-continuous/terraform/compare/3.9.0...3.9.1) (2023-04-27)


### Bug Fixes

* **tflint:** add recursive option to default args ([b5270f7](https://gitlab.com/to-be-continuous/terraform/commit/b5270f7d9b4b6a1b73d16f773a1ffa7d2db346c0))

# [3.9.0](https://gitlab.com/to-be-continuous/terraform/compare/3.8.0...3.9.0) (2023-03-20)


### Features

* add terraform validate job ([24de67c](https://gitlab.com/to-be-continuous/terraform/commit/24de67cf1df4e2281c4d6d5d3f90df2324f1a23f))

# [3.8.0](https://gitlab.com/to-be-continuous/terraform/compare/3.7.0...3.8.0) (2023-03-11)


### Bug Fixes

* **workspace:** unset $TF_WORKSPACE to avoid conflict with Terraform ([28c128e](https://gitlab.com/to-be-continuous/terraform/commit/28c128e8993ba19221bad252f935a8e29e24603a))


### Features

* **workspace:** select or create ([d94c4b0](https://gitlab.com/to-be-continuous/terraform/commit/d94c4b0ce58fa064c2eeda445e3aee350e0a8a96))
* **workspace:** support auto value ([ae5fbdf](https://gitlab.com/to-be-continuous/terraform/commit/ae5fbdf1a151b09e7c1d80fb16f2d7011246f0d3))

# [3.7.0](https://gitlab.com/to-be-continuous/terraform/compare/3.6.3...3.7.0) (2023-03-10)


### Features

* add support for terraform workspaces ([e7e0d50](https://gitlab.com/to-be-continuous/terraform/commit/e7e0d50a3ce56f78e23aecc8fa2597b4d895af6d)), closes [#45](https://gitlab.com/to-be-continuous/terraform/issues/45)

## [3.6.3](https://gitlab.com/to-be-continuous/terraform/compare/3.6.2...3.6.3) (2023-02-19)


### Bug Fixes

* improve logs when [@url](https://gitlab.com/url)@ variable eval fails ([aa396f2](https://gitlab.com/to-be-continuous/terraform/commit/aa396f2936e5e6567d58bbbff7617c3784173823))

## [3.6.2](https://gitlab.com/to-be-continuous/terraform/compare/3.6.1...3.6.2) (2023-01-27)


### Bug Fixes

* **gcp:** remove enforced var ([6c2fae1](https://gitlab.com/to-be-continuous/terraform/commit/6c2fae1bc2ec90807fa0f865f8fc9007990390ce))

## [3.6.1](https://gitlab.com/to-be-continuous/terraform/compare/3.6.0...3.6.1) (2023-01-26)


### Bug Fixes

* Add registry name in all Docker images ([e99d316](https://gitlab.com/to-be-continuous/terraform/commit/e99d3165b737a7c284c51b5af8558d0b04d24d63))

# [3.6.0](https://gitlab.com/to-be-continuous/terraform/compare/3.5.1...3.6.0) (2023-01-13)


### Features

* use new tfsec image as default ([8afc1e1](https://gitlab.com/to-be-continuous/terraform/commit/8afc1e1b91b355e4e362ec21bce84a13fadbec05))

## [3.5.1](https://gitlab.com/to-be-continuous/terraform/compare/3.5.0...3.5.1) (2023-01-10)


### Bug Fixes

* change the way to TRACE tflint ([f2761e3](https://gitlab.com/to-be-continuous/terraform/commit/f2761e3aa6a7fdb4895934329216daf172c4c7e3))

# [3.5.0](https://gitlab.com/to-be-continuous/terraform/compare/3.4.0...3.5.0) (2023-01-05)


### Features

* add GCP Auth provider variant ([4a8d78e](https://gitlab.com/to-be-continuous/terraform/commit/4a8d78e8b1cb7f661604a43a51807a2f7b89d81d))

# [3.4.0](https://gitlab.com/to-be-continuous/terraform/compare/3.3.0...3.4.0) (2022-12-30)


### Features

* **job:** add "needs" for Jobs with no dependency to make the CI faster ([e6cd115](https://gitlab.com/to-be-continuous/terraform/commit/e6cd11561ea1b0efc9bf91a5c9b8e77cbcf91e20))

# [3.3.0](https://gitlab.com/to-be-continuous/terraform/compare/3.2.1...3.3.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([93c60c4](https://gitlab.com/to-be-continuous/terraform/commit/93c60c4376eec306fd90ea0f0a9047c2c79f94ba))

## [3.2.1](https://gitlab.com/to-be-continuous/terraform/compare/3.2.0...3.2.1) (2022-10-19)


### Bug Fixes

* -var and -var-file options cannot be used when applying a saved plan ([7a345de](https://gitlab.com/to-be-continuous/terraform/commit/7a345dea00c29c9cbef68e0a8057229ec3ee586d))

# [3.2.0](https://gitlab.com/to-be-continuous/terraform/compare/3.1.0...3.2.0) (2022-10-18)


### Features

* **job:** add terraform fmt checker ([08a1ee3](https://gitlab.com/to-be-continuous/terraform/commit/08a1ee3bf5eb86d46e5917ad4793fb87a249ec8a)), closes [#37](https://gitlab.com/to-be-continuous/terraform/issues/37)

# [3.1.0](https://gitlab.com/to-be-continuous/terraform/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([b2fe717](https://gitlab.com/to-be-continuous/terraform/commit/b2fe717285d39f066ef3d0d311dd47cb8b3c30b3))

# [3.0.0](https://gitlab.com/to-be-continuous/terraform/compare/2.8.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([735514e](https://gitlab.com/to-be-continuous/terraform/commit/735514e134f718fdd757b540d1ae83c2c69f0f9a))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.8.0](https://gitlab.com/to-be-continuous/terraform/compare/2.7.1...2.8.0) (2022-06-29)


### Features

* add implicit Backend configuration ([66cf555](https://gitlab.com/to-be-continuous/terraform/commit/66cf55570075d86c5b83a09bbee65f7dba679ea4))
* add implicit tfvars support ([eada2c7](https://gitlab.com/to-be-continuous/terraform/commit/eada2c7ba899b7083e3a2efadbbac7cfbb73b2cf))

## [2.7.1](https://gitlab.com/to-be-continuous/terraform/compare/2.7.0...2.7.1) (2022-05-25)


### Bug Fixes

* install curl when not found for DELETE method ([ac6ff96](https://gitlab.com/to-be-continuous/terraform/commit/ac6ff9651867b1ddd53add17ee006a5e756cf7e9))

# [2.7.0](https://gitlab.com/to-be-continuous/terraform/compare/2.6.0...2.7.0) (2022-05-13)


### Features

* remove GitLab managed Terraform state on environment deletion ([e4232e6](https://gitlab.com/to-be-continuous/terraform/commit/e4232e6412295cc2a8d9c75200277030b5c4b8cd))

# [2.6.0](https://gitlab.com/to-be-continuous/terraform/compare/2.5.0...2.6.0) (2022-05-09)


### Features

* add JSON format for TFSec output ([34c8f41](https://gitlab.com/to-be-continuous/terraform/commit/34c8f4139909125f13c6435e66eb66b811f7c21a))

# [2.5.0](https://gitlab.com/to-be-continuous/terraform/compare/2.4.3...2.5.0) (2022-05-01)


### Features

* configurable tracking image ([163c306](https://gitlab.com/to-be-continuous/terraform/commit/163c3069be2e3c23fa72961dfa318b7ceb47c4b8))

## [2.4.3](https://gitlab.com/to-be-continuous/terraform/compare/2.4.2...2.4.3) (2022-04-27)


### Bug Fixes

* avoid merge request pipelines ([4ecf4e5](https://gitlab.com/to-be-continuous/terraform/commit/4ecf4e5826e745dd6a2d401dd4c2a71a97ac1598))

## [2.4.2](https://gitlab.com/to-be-continuous/terraform/compare/2.4.1...2.4.2) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([ef9078f](https://gitlab.com/to-be-continuous/terraform/commit/ef9078f350d40603a979367edf76d57c0b08f8ff))

## [2.4.1](https://gitlab.com/to-be-continuous/terraform/compare/2.4.0...2.4.1) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([5732c06](https://gitlab.com/to-be-continuous/terraform/commit/5732c06f79873ba5e1fa3506dd4f29484891ba80))

# [2.4.0](https://gitlab.com/to-be-continuous/terraform/compare/2.3.2...2.4.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([4f1265a](https://gitlab.com/to-be-continuous/terraform/commit/4f1265a39edd87fcac848778d2f687661e527421))

## [2.3.2](https://gitlab.com/to-be-continuous/terraform/compare/2.3.1...2.3.2) (2021-12-21)


### Bug Fixes

* **ci:** only launch production plan when merge request targets the prod branch ([898275e](https://gitlab.com/to-be-continuous/terraform/commit/898275e7fc5298621a34fc7ca2349ab7898befe2))

## [2.3.1](https://gitlab.com/to-be-continuous/terraform/compare/2.3.0...2.3.1) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([f552ee2](https://gitlab.com/to-be-continuous/terraform/commit/f552ee24d31d5d38cb68717008fe24bee0f374b7))

# [2.3.0](https://gitlab.com/to-be-continuous/terraform/compare/2.2.4...2.3.0) (2021-10-18)


### Features

* add plugin for cloud providers ([370c141](https://gitlab.com/to-be-continuous/terraform/commit/370c141bfeaab4cfa6d3bf7b179a2a5de2e2e8ad))

## [2.2.4](https://gitlab.com/to-be-continuous/terraform/compare/2.2.3...2.2.4) (2021-10-18)


### Bug Fixes

* revert test jobs on change only ([53378e4](https://gitlab.com/to-be-continuous/terraform/commit/53378e4e3b18f9d914e3c621d970a18b2a09edfb))

## [2.2.3](https://gitlab.com/to-be-continuous/terraform/compare/2.2.2...2.2.3) (2021-10-15)


### Bug Fixes

* update tflint image ([78014a7](https://gitlab.com/to-be-continuous/terraform/commit/78014a766ffc6543d6d2c71efa67b92e8f590eaf))

## [2.2.2](https://gitlab.com/to-be-continuous/terraform/compare/2.2.1...2.2.2) (2021-10-15)


### Bug Fixes

* enable change on subdir ([e80d07d](https://gitlab.com/to-be-continuous/terraform/commit/e80d07d39f14e91954c69568d26178b34aa2c6c3))

## [2.2.1](https://gitlab.com/to-be-continuous/terraform/compare/2.2.0...2.2.1) (2021-10-13)


### Bug Fixes

* only launch tests job on code update ([38b1143](https://gitlab.com/to-be-continuous/terraform/commit/38b114355805491bc03e7f12961775a086485808))

# [2.2.0](https://gitlab.com/to-be-continuous/terraform/compare/2.1.1...2.2.0) (2021-10-13)


### Features

* allow to override Terraform commands using a GitLab reference feature. ([64bed8a](https://gitlab.com/to-be-continuous/terraform/commit/64bed8a7973f133056542a131648b542479456e5))

## [2.1.1](https://gitlab.com/to-be-continuous/terraform/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([fb622a1](https://gitlab.com/to-be-continuous/terraform/commit/fb622a1a34d5d77d4de4e472e77ebdb50977999d))

# [2.1.0](https://gitlab.com/to-be-continuous/terraform/compare/2.0.0...2.1.0) (2021-09-19)


### Features

* add infracost ([1ffba7f](https://gitlab.com/to-be-continuous/terraform/commit/1ffba7f5eb3679c8c314d6e9fb8a696f31525a09))

## [2.0.0](https://gitlab.com/to-be-continuous/terraform/compare/1.1.2...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([f175aed](https://gitlab.com/to-be-continuous/terraform/commit/f175aed5f75a03802e70e7736c429c132e8ca565))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.1.2](https://gitlab.com/to-be-continuous/terraform/compare/1.1.1...1.1.2) (2021-07-21)

### Bug Fixes

* remove dependencies on tfsec, tflint and checkcov ([f8f1e87](https://gitlab.com/to-be-continuous/terraform/commit/f8f1e871574c8a14ae36002efe2d9a0288476ad0))

## [1.1.1](https://gitlab.com/to-be-continuous/terraform/compare/1.1.0...1.1.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([27ebea7](https://gitlab.com/to-be-continuous/terraform/commit/27ebea71b1f872200ccf3f0d1d7c08f1fb9d8e3d))

## [1.1.0](https://gitlab.com/to-be-continuous/terraform/compare/1.0.0...1.1.0) (2021-06-10)

### Features

* move group ([869658c](https://gitlab.com/to-be-continuous/terraform/commit/869658cd53feb93b984028556114c318f872fc9b))

## 1.0.0 (2021-05-06)

### Features

* initial release ([9421d94](https://gitlab.com/Orange-OpenSource/tbc/terraform/commit/9421d94e6fd8149da03831dcda3723baaed7d745))
